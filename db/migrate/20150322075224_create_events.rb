class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.datetime :event_date
      t.integer :ticket_fee
      t.string :venue

      t.timestamps null: false
    end
  end
end
